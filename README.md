# [Chat](https://github.com/I-Atlas/chat-client)
![GitHub](https://img.shields.io/github/license/I-Atlas/chat-client?style=flat-square)
[![Iliya Bolotov](https://img.shields.io/badge/iliya-bolotov-%23ff6f61?style=flat-square&logo=appveyor)](https://github.com/I-Atlas)

<a href="https://github.com/I-Atlas/chat-client">
    <img src="src/assets/img/chat.png" alt="Arkanoid">
</a>

Simple chat application.

Basic functioal:
- auth
- start chat with users
- send images, emojis, voice and text messages

You can find the api for this project [here](https://github.com/I-Atlas/chat-server).
## 👨‍💻 Technologies used
- JavaScript
- React
- Redux / Thunk
- Formik
- Socket.io
- Ant Design
- JWT

## 🚀 Getting started
### 🧰 Setup
```
npm install && npm start
```
```
yarn && yarn start
```
## 📄 License
The project is licensed under the [Apache license 2.0](https://github.com/I-Atlas/chat-client/blob/main/LICENSE).